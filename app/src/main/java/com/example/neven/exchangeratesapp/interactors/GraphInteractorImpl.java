package com.example.neven.exchangeratesapp.interactors;

import com.example.neven.exchangeratesapp.listeners.GraphListener;
import com.example.neven.exchangeratesapp.models.ExchangeRate;
import com.example.neven.exchangeratesapp.network.RestAPI;
import com.example.neven.exchangeratesapp.utils.DateUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import javax.inject.Inject;
import java.util.*;

/**
 * Created by Neven on 29.6.2017..
 */
public class GraphInteractorImpl implements GraphInteractor {

    private final Retrofit retrofit;
    private final DateUtils dateUtils;
    private List<ExchangeRate> listExchangeRates;


    @Inject

    public GraphInteractorImpl(Retrofit retrofit, DateUtils dateUtils) {
        this.retrofit = retrofit;
        this.dateUtils = dateUtils;
    }

    @Override
    public void downloadData(GraphListener listener, String selectedCurrency) {

        RestAPI api = retrofit.create(RestAPI.class);

        List<String> listDates = dateUtils.getDates();  // this list contains last 7 days.

        List<Observable<List<ExchangeRate>>> listObservables = new ArrayList<>();
        listExchangeRates = new ArrayList<>();

        for (String date : listDates) {

            Observable<List<ExchangeRate>> observable = api.getExchangeRatesForLast7days(date);
            listObservables.add(observable);

        }


        Observable<List<ExchangeRate>> zipObservable = Observable.zipIterable(listObservables, objects -> {

            for (Object singleObject : objects) {

                @SuppressWarnings("unchecked")
                List<ExchangeRate> singleList = (List<ExchangeRate>) singleObject;

                for (ExchangeRate singleRate : singleList) {

                    if (singleRate.currencyCode.equalsIgnoreCase(selectedCurrency)) {
                        listExchangeRates.add(singleRate);

                    }
                }
            }

            return listExchangeRates;

        }, true, 10);

        zipObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        listener::onSuccess,
                        Throwable::printStackTrace
                );


    }

}
