package com.example.neven.exchangeratesapp.views;

import com.example.neven.exchangeratesapp.models.ExchangeRate;

import java.util.List;

/**
 * Created by Neven on 15.6.2017..
 */
public interface GraphView {

    void showGraph(List<ExchangeRate> listRates);

    void onCurrenciesAdded(List<String> listCurrencies);

    void showErrorMessage(String message);


}
